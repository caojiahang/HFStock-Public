# -*- coding: utf-8 -*-
# coding: utf-8

import os
import sys
import numpy as np
import time

import backtest
o_path = os.path.abspath(os.path.dirname(os.getcwd()))
sys.path.append(o_path + '\\LSTM')
import predict
import config
from data_prepare import data_normalize

day_period = config.day_period
train_stocks = config.train_stocks  # 是否重复调用get_hs_300？
features = config.features
n_steps = config.n_steps
n_inputs = config.n_inputs
average_point = config.average_point


def trans_data(data, train_stocks, day_period):
    stock_list = list()
    data_list = list()
    for stock in train_stocks:
        if True in [data[str(day)][stock]['empty'] for day in range(-day_period + 1, 1)]:
            continue
        stock_list.append(stock)
        tmp_list = [[list(data[str(day)][stock][feature]) for feature in data[str(day)][stock].keys() if feature in features]
                    for day in range(-day_period + 1, 1)]
        data_list.append(tmp_list)
        # 四维：stock * day * feature * steps
    data_array = np.array(data_list, dtype=np.float32)
    # 维数变化
    data_array = data_array.transpose((0, 1, 3, 2))  # 如果有可能某天股票池所有股票停牌，则data_list为空，出bug
    return data_array, stock_list


class Template(backtest.BackTest):
    def __init__(self):
        # general information
        time_begin = '2019-01-02'
        time_end = '2019-05-31'
        initial_num = 9
        strategy_name = 'final'
        slippage_num = 1.0

        # variables
        self.stocks_buy = []

        # visualization
        is_figure_report = True
        evaluation_fig_size = 30
        eda_list = None
        factor_list = None
        is_twinx = False
        is_interest = True
        is_candlestick = False
        is_volume = False

        super().__init__(time_begin, time_end, initial_num, strategy_name, slippage_num, is_figure_report,
                        evaluation_fig_size, eda_list, is_twinx, is_interest,
                        is_candlestick, is_volume, factor_list)

    def strategy(self):
        print(str(self.num))

        data_long = self.get_kline('120m', 10)
        # 数据结构 dict(day:)->dict(stock:)->dict(feature:features_array）
        data_long_array, stock_list = trans_data(data_long, train_stocks, 10)
        data_long_array = data_long_array.reshape(data_long_array.shape[0], 20, n_inputs)
        data_long_array = data_normalize(data_long_array)

        data_short = self.get_kline('15m', 1)
        data_short_array, stock_list = trans_data(data_short, stock_list, 1)
        data_short_array = data_short_array.reshape(data_short_array.shape[0], 16, n_inputs)
        data_short_array = data_normalize(data_short_array)
        '''
        np.set_printoptions(suppress=True)
        np.set_printoptions(threshold=np.inf)
        print(data_array)
        print(stock_list)
        '''
        pred_long = predict.predict(data_long_array, 20)
        pred_short = predict.predict(data_short_array, 16)

        expectation = dict()
        for i, s in enumerate(stock_list):
            exp = 0
            for j in range(4):
                exp += average_point[str(j)] * pred_short[i][j] * 0.5 + average_point[str(j)] * pred_long[i][j] * 0.5
            expectation[s] = exp
        tmp = sorted(expectation.items(), reverse=True, key=lambda a: a[1])
        stocks_to_buy = list()
        for i, t in enumerate(tmp):
            if float(t[1]) < 0.0014 or i >= 20:
                break
            else:
                stocks_to_buy.append(t[0])
        self.stocks_buy = stocks_to_buy
        print('stock_buy: ', self.stocks_buy)


if __name__ == '__main__':
    strategy = Template()
    strategy.run()
