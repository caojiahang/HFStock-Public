import os
import tensorflow as tf

import config

# 关于运算图和网络结构的参数配置
n_inputs = config.n_inputs
n_neurons = config.n_neurons
n_label = config.n_label
train_stocks = config.train_stocks
s_point = config.s_point
average_point = config.average_point

# 训练出的模型位置
model_path_long = 'final_model/long/search_model.ckpt'
model_path_short = 'final_model/short/search_model.ckpt'


# 日期包含begin 不包含end 且data仅是一天的数据 二维array
def predict(data, n_steps):
    tf.reset_default_graph()
    if n_steps == 20:
        model_path = model_path_long
    else:
        model_path = model_path_short
    x = tf.compat.v1.placeholder(tf.float32, [None, n_steps, n_inputs], name='x')  # 测试集size未知
    in_prob = tf.compat.v1.placeholder(tf.float32, [], name='in_prob')
    out_prob = tf.compat.v1.placeholder(tf.float32, [], name='out_prob')

    lstm_layer_1 = tf.contrib.rnn.LSTMCell(num_units=n_neurons[0], use_peepholes=True, state_is_tuple=False)
    lstm_layer_2 = tf.contrib.rnn.LSTMCell(num_units=n_neurons[1], use_peepholes=True, state_is_tuple=False)
    layer_2_dropout = tf.contrib.rnn.DropoutWrapper(lstm_layer_2, input_keep_prob=out_prob, output_keep_prob=in_prob)
    cell = tf.nn.rnn_cell.MultiRNNCell([lstm_layer_1, layer_2_dropout], state_is_tuple=False)
    outputs, _ = tf.nn.dynamic_rnn(cell, x, dtype=tf.float32)
    logits = tf.layers.dense(outputs[:, -1], n_label)  # 这里的全连接层是没有激活函数的，相当于做了一个线性映射以改变输出维度
    prediction = tf.nn.softmax(logits)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        init.run()
        o_path = os.path.abspath(os.path.dirname(os.getcwd()))
        saver.restore(sess, o_path + '\\LSTM\\' + model_path)
        pred = prediction.eval(feed_dict={x: data, in_prob: 1.0, out_prob: 1.0})
        return pred
