import config
import data_prepare
import search_hyparameter
import train_LSTM_model

import numpy as np


# 参数配置
frequency = config.frequency
n_inputs = config.n_inputs
n_steps = config.n_steps
n_neurons = config.n_neurons
learning_rate = config.learning_rate
batch_size = config.batch_size
n_label = config.n_label
n_epoch = config.n_epoch
train_scale = config.train_scale
keep_in_prob = config.keep_in_prob
keep_out_prob = config.keep_out_prob
day_period = config.day_period
d_begin = config.d_begin
d_end = config.d_end
features = config.features
train_stocks = config.train_stocks
s_point = config.s_point
average_point = config.average_point


optimizer_grid = ['Adam', 'RMSProp', 'Adadelta']
regularization_grid = ['Dropout', 'L2_regularization']
L2_grid = ['0.001', '0.0005', '0.0002']
drop_grid = ['1', '2', '3', '4', '5']
# 1: in0.5*out0.5——2: in0.6*out0.6——3: in0.7*out0.7——4: in0.8*out0.8——5：in0.4*out0.4
batch_grid = ['20', '40']
np.set_printoptions(suppress=True)
np.set_printoptions(threshold=np.inf)

train_LSTM_model.train_lstm_model(16, n_inputs, n_neurons, n_label, learning_rate, d_begin, d_end, 15,
                                  train_stocks, features, s_point, train_scale, batch_size, n_epoch,
                                  keep_in_prob, keep_out_prob, 1)
train_LSTM_model.train_lstm_model(20, n_inputs, n_neurons, n_label, learning_rate, d_begin, d_end, 120,
                                  train_stocks, features, s_point, train_scale, batch_size, n_epoch,
                                  keep_in_prob, keep_out_prob, 10)
'''
data_x, data_y = data_prepare.fetch_data(d_begin, d_end, frequency, train_stocks, features, s_point, day_period)
data_x = data_x.reshape(data_x.shape[0], n_steps, n_inputs)
data_x = data_prepare.data_normalize(data_x)
# LSTM模型只需要三维数据，将日内time_steps与ay_period两个维度合并
train_x, train_y, test_x, test_y = data_prepare.data_seperate(data_x, data_y, train_scale)
'''
'''
for op_ch in optimizer_grid:
    search_hyparameter.search_hyparameter(train_x, train_y, test_x, test_y, op_ch, 'Dropout', 'None', '2',
                                          '30', n_steps, n_inputs, n_neurons, n_label, learning_rate,
                                          n_epoch)
for l2_ch in L2_grid:
    search_hyparameter.search_hyparameter(train_x, train_y, test_x, test_y, 'Adam', 'L2_regularization', l2_ch, 'None',
                                          '30', n_steps, n_inputs, n_neurons, n_label, learning_rate,
                                          n_epoch)
for dr_ch in drop_grid:
    search_hyparameter.search_hyparameter(train_x, train_y, test_x, test_y, 'Adam', 'Dropout', 'None', dr_ch,
                                          '30', n_steps, n_inputs, n_neurons, n_label, learning_rate,
                                          n_epoch)
'''
'''
# 网格搜索超参数：
data_x, data_y = data_prepare.fetch_data(d_begin, d_end, frequency, train_stocks, features, s_point, day_period)
train_x, train_y, test_x, test_y = data_prepare.data_seperate(data_x, data_y, train_scale)
for op_ch in optimizer_grid:
    for re_ch in regularization_grid:
        for l2_ch in L2_grid:
            for dr_ch in drop_grid:
                for ba_ch in batch_grid:
                    search_hyparameter.search_hyparameter(train_x, train_y, test_x, test_y, op_ch, re_ch, l2_ch, dr_ch, 
                                                          ba_ch, n_steps, n_inputs, n_neurons, n_label, learning_rate, 
                                                          n_epoch, day_period)
'''

'''
train_LSTM_model.train_lstm_model(n_steps, n_inputs, [80, 50], n_label, learning_rate, d_begin, d_end, frequency,
                                  train_stocks, features, s_point, train_scale, batch_size, 40,
                                  keep_in_prob, keep_out_prob, day_period)
'''
'''
train_stocks = get_stock_code.get_zz_800()
train_LSTM_model.train_lstm_model(n_steps, n_inputs, [100, 60], n_label, learning_rate, d_begin, d_end, frequency,
                                  train_stocks, features, s_point, train_scale, batch_size, 110,
                                  keep_in_prob, keep_out_prob, day_period)
'''
'''
train_LSTM_model.train_lstm_model(n_steps, n_inputs, n_neurons, n_label, learning_rate, d_begin, d_end, frequency,
                                  train_stocks, features, s_point, train_scale, batch_size, n_epoch,
                                  keep_in_prob, keep_out_prob, day_period)
'''
