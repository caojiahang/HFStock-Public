import data_prepare
import get_stock_code
import search_hyparameter
import train_LSTM_model

import numpy as np
from math import floor

# attention: 使用predict时需先将search 和 train 注释掉

# 参数配置
frequency = 15
n_inputs = 11
n_outputs = 1
n_steps = 16
n_neurons = [80, 50]
learning_rate = 0.001
batch_size = 30
n_label = 4
n_epoch = 20
train_scale = 0.8
keep_in_prob = 0.7
keep_out_prob = 0.7
day_period = 1
d_begin = '2019-04-19'
d_end = '2019-05-14'
features = ['收盘价', '开盘价', '最高价', '最低价', '成交量', '成交金额', '成交笔数', '委比', '量比', '委买', '委卖']
# train_stocks = get_stock_code.get_hs_300()
train_stocks = ['SH600282']
s_point, average_point = data_prepare.labels_select(train_stocks)  # 可改进为p分位数

# 网格数据：
optimizer_grid = ['Adam', 'RMSProp', 'Adadelta']
regularization_grid = ['Dropout', 'L2_regularization']
L2_grid = ['0.001', '0.0005', '0.0002']
drop_grid = ['1', '2', '3', '4', '5']  # 1: in0.7*out0.7——2: in0.5——3: out0.5——4: in0.8*out0.8——5: in0.6*out0.6
batch_grid = ['20', '40']

np.set_printoptions(suppress=True)
np.set_printoptions(threshold=np.inf)
data_x, data_y = data_prepare.fetch_data(d_begin, d_end, frequency, train_stocks, features, s_point,
                                         day_period)
print(data_x, data_y)
n_batch = floor(len(data_x) / batch_size)
train_x, train_y = data_prepare.fetch_train_batch(data_x, data_y, batch_size, n_batch, n_steps, n_inputs, day_period)
print(train_x, train_y)
