import pandas as pd
import numpy as np
import os


def get_dirs(path, date_begin, date_end, days=1):
    name = []
    for _, dirs, _ in os.walk(path):
        flag = False
        for i in dirs:
            if (flag == False) & (i == date_begin):
                name.append(i)
                flag = True
            elif (flag == True) & (i != date_end):
                name.append(i)
            elif i == date_end:
                break
    return name


def get_files(path, filetype):
    name = []
    for _, _, files in os.walk(path):
        for i in files:
            if filetype in i:
                name.append(i.replace('.csv', ''))
    return name


def data_load(stock, d_begin, d_end, days=1):
    dates = get_dirs(path='../data/' + '120m', date_begin=d_begin, date_end=d_end)
    data = pd.DataFrame()
    for i in range(len(dates)):
        data_df = pd.read_csv('../data/' + '120m' + '/' + dates[i] + '/' + stock + '.csv', index_col=False,
                              encoding='ANSI')
        data = pd.concat([data, data_df])
    return data


x = data_load('SH600000', '2019-01-02','2019-04-08')
print(x)
