# -*- coding: utf-8 -*-
# coding: utf-8

import numpy as np
import pandas as pd
from scipy.stats import rankdata

win_c = 3
per_c = 1


def ts_sum(df, window=win_c):
    return df.rolling(window).sum()


def sma(df, window=win_c):
    return df.rolling(window).mean()


def stddev(df, window=win_c):
    return df.rolling(window).std()


def correlation(df1, df2, window=win_c):
    return df1.rolling(window).corr(df2)


def covariance(df1, df2, window=win_c):
    return df1.rolling(window).cov(df2)


def ts_min(df, window=win_c):
    return df.rolling(window).min()


def ts_max(df, window=win_c):
    return df.rolling(window).max()


def ts_argmin(df, window=win_c):
    return df.rolling(window).apply(np.argmin, raw=True) + 1


def ts_argmax(df, window=win_c):
    return df.rolling(window).apply(np.argmin, raw=True) + 1


def product(df, window=win_c):
    return df.rolling(window).apply(np.prod, raw=True)


def ts_rank(df, window=win_c):
    return df.rolling(window).apply(np.rankdata[-1])


def delta(df, period=per_c):
    return df.diff(period)


def delay(df, period=per_c):
    return df.shift(period)


def rank(df):
    return df.rank(pct=True)


def decay_linear(df1, window=win_c):
    df = df1.astype('float')
    s = float(sum(range(window+1)))
    for y in range(df.shape[1]):
        for x in range(df.shape[0]):
            if df.shape[0]-x-1-window+1 < 0:
                pass
            else:
                df.iat[df.shape[0] - x - 1, y] *= window/s
                for t in range(window-1):
                    df.iat[df.shape[0]-x-1, y] += df.iat[df.shape[0]-x-1-window+t+1, y]*((t+1)/s)
    return df


def scale(df, k=1):
    return df.mul(k).div(np.abs(df).sum())
