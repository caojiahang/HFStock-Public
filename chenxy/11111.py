import matplotlib.pyplot as plt
import numpy as np

cm = np.array([[1,2,3,4],[2,3,4,5],[3,4,5,6],[4,5,6,7]])


def plot_confusion_matrix(cm, labels_name, way_to_normalize='index'):
    if way_to_normalize == 'index':
        cm_float = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]    # 行归一化（一般不会出现分母为0）但测试时要避免
    elif way_to_normalize == 'columns':
        cm_float = cm.astype('float') / cm.sum(axis=0)[np.newaxis, :]  # 列归一化
    plt.imshow(cm_float, interpolation='nearest')
    plt.title('Confusion Matrix Normalized by ' + way_to_normalize)    # 图像标题
    plt.colorbar()
    num_local = np.array(range(len(labels_name)))
    plt.xticks(num_local, labels_name)    # 将标签印在x轴坐标上
    plt.yticks(num_local, labels_name)    # 将标签印在y轴坐标上
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            plt.text(x=i, y=j, s=int(cm[i][j]), va='center', ha='center', color='red', fontsize=15)
    plt.savefig('search_log/' + way_to_normalize + '.jpg')  # w+b 没办法创建新文件夹？
    plt.close()


plot_confusion_matrix(cm, ['0','1','2','3'], 'index')
plot_confusion_matrix(cm, ['0','1','2','3'], 'columns')




