import matplotlib.pylab
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

df = pd.Series(np.random.randn(600), index=pd.date_range('7/1/2016', freq='D', periods=600))

r = df.rolling(window=10)

plt.figure(figsize=(15, 5))

df.plot(style='r--')
df.rolling(window=10).mean().plot(style='b')
plt.show()