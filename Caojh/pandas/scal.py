import pandas as pd
import numpy as np
def scale(df, k=1):
    return df.mul(k).div(np.abs(df).sum())
df=pd.DataFrame([[1,2,3],[2,5,6],[3,1,1]],index=['one','two','three'],columns=['A','B','C'])
print(scale(df,1))