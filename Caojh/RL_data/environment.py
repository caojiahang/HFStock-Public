import numpy as np
import pandas as pd
import time
import random
from pandas import DataFrame
import os

class Environment:
    def __init__(self):

        print(">> Initialize the Environment.")

    #Reset the state of the environment
    def reset(self):
        self.balance = INITIAL_PROPERTY


    #Obtain the start_time & end_time
    def organ_data(self,start_date,end_date,codes_num):

        #read data
        self.data = pd.read_csv('../data/'+'5m/'+'2019-01-02'+'.csv')
        codes_data_list = get_files(path='../data/'+'5m/'+'2019-01-02',filetype='csv')  #读取所有股票代码
        codes_data = DataFrame(codes_data_list)
        #随机抽取几只股票并提取它们的数据
        sample_flag = True
        while sample_flag:
            codes = random.sample(set(codes_data),codes_num)
            data2 = self.data.loc[codes_data.isin(codes)]

            # 生成有效时间
            date_set = set(data2['时间'])
            for code in codes:
                date_set = date_set.intersection((set(data2['时间'])))
            if len(date_set) > 500:
                sample_flag = False

        date_set = date_set.intersection(set(pd.date_range(start_date, end_date)))
        self.date_set = list(date_set)
        self.date_set.sort()

        train_start_time = self.date_set[0]
        train_end_time = self.date_set[int(len(self.date_set) / 6) * 5 - 1]
        test_start_time = self.date_set[int(len(self.date_set) / 6) * 5]
        test_end_time = self.date_set[-1]

        return train_start_time,train_end_time,test_start_time,test_end_time,codes


    #提取数据并定义初始状态
    def get_states(self,start_time,end_time,window_len,codes):

        self.codes = codes
        codes_data_list = get_files(path='../data/' + '5m/' + '2019-01-02', filetype='csv')  # 读取所有股票代码

        #Initialize the state
        self.date_set = pd.date_range(start_time,end_time)
        date_set = self.date_set
        self.L = window_len
        self.t1 = 0
        self.t2 = self.L
        t1 = self.t1
        t2 = self.t2
        codes_data_list = get_files(path='../data/' + '5m/' + '2019-01-02', filetype='csv')

        dateset = get_dir(path='../data/' + '5m')
        codes = ['SH600015', 'SH600012']
        data = pd.read_csv('../data/' + '5m/' + dateset[0] + '/' + codes[0] + '.csv', encoding='gbk')
        data_all = stack_list(data)
        for code in codes:
            for time in dateset[t1:t2]:
                data = pd.read_csv('../data/' + '5m/' + time + '/' + code + '.csv', encoding='gbk')
                data = stack_list(data)
                if code == codes[t1] and time == dateset[t1]:
                    pass
                else:
                    data_all = np.concatenate((data_all, data), axis=0)
        states = np.reshape(data_all, (2, 480, 6))
        return states

    def takeAction(self,action):
        c = DataFrame(self.states)
        Current_Close = c.loc[47:49,2])    #Read the current_close
        l = DataFrame(self.states)
        Lasttime_Close = l.loc[47:49,2])    #Read the lasttime_close
        Current_Balance = self.balance
        Positive_Amount = action.count(1)
        #Negative_Amount = action2.count(0)
        Average_Price = CurrentBalance/PositiveAmount
        if action = 1:
            Next_Balance = Average_Price * (Current_Close/Lasttime_Close)
        self.reward = Next_Balance/Current_Balance


    def step(self,action):
        self.takeAction(action)
        self.t1+=1
        self.t2+=1
        if self.t1 > (self.date_set - self.L)
            self.t1 = 0

        reward = self.reward
        return reward

    def reset(self):
        self.t = self.L + 1






















def stack_list(self,data):
    C_name = np.vstack(data['代码'])
    C_time = np.vstack(data['时间'])
    C_close = np.vstack(data['收盘价'])
    C_open = np.vstack(data['开盘价'])
    C_high = np.vstack(data['最高价'])
    C_low = np.vstack(data['最低价'])
    data_list = np.hstack((C_name, C_time, C_close, C_open, C_high, C_low))
    return data_list

def get_dir(path):
    name = []
    for _, dir, _, in os.walk(path):
        for i in dir:
            name.append(i)
    return name


def get_files(path, filetype):
    name = []
    for _, _, files in os.walk(path):
        for i in files:
            if filetype in i:
                name.append(i.replace('.csv', ''))
    return name