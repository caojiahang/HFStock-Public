import numpy as np
import pandas as pd
import time
import random
from pandas import DataFrame
import os

def get_dir(path):
    name = []
    for _, dir, _, in os.walk(path):
        for i in dir:
            name.append(i)
    return name


def get_files(path, filetype):
    name = []
    for _, _, files in os.walk(path):
        for i in files:
            if filetype in i:
                name.append(i.replace('.csv', ''))
    return name

def stack_list(data):
    C_name = np.vstack(data['代码'])
    C_time = np.vstack(data['时间'])
    C_close = np.vstack(data['收盘价'])
    C_open = np.vstack(data['开盘价'])
    C_high = np.vstack(data['最高价'])
    C_low = np.vstack(data['最低价'])
    data_list = np.hstack((C_name, C_time, C_close, C_open, C_high, C_low))
    return data_list


def get_detail(data):
    C_close = np.vstack(data['收盘价'])
    detail = np.hstack((C_close))
    return detail


def get_states(start_time,end_time):
    codes_data_list = get_files(path='../data/' + '5m/' + '2019-01-02', filetype='csv')
    dateset = get_dir(path='../data/' + '5m')
    codes = ['SH600012','SH600015']

    data = pd.read_csv('../data/' + '5m/' + dateset[0] + '/' + codes[0] + '.csv', encoding='gbk')
    detail = get_detail(data)
    data_all = stack_list(data)

    t1 = start_time
    t2 = end_time
    for code in codes:
        for time in dateset[t1:t2]:
            data = pd.read_csv('../data/' + '5m/' + time + '/' + code + '.csv', encoding='gbk')
            data = stack_list(data)
            if code==codes[t1] and time==dateset[t1]:
                pass
            else:
                data_all= np.concatenate((data_all,data),axis=0)
    states=np.reshape(data_all,(2,480,6))
    return states,detail

#action = [1,0]
states = get_states(0, 10)
print(states)

#c = DataFrame(states)
#print(c)
# Current_Close = c.loc[47:49,2])    #Read the current_close
# l = DataFrame(states)
# Lasttime_Close = l.loc[47:49,2])    #Read the lasttime_close
# Current_Balance = 1
# Positive_Amount = action.count(1)
# #Negative_Amount = action2.count(0)
# Average_Price = CurrentBalance/PositiveAmount
# if action = 1:
#     Next_Balance = Average_Price * (Current_Close/Lasttime_Close)
# self.reward = Next_Balance/Current_Balance


