import numpy as np
import pandas as pd
import os

def data_load(data_time):

    # read stock names
    stocks = get_files(path='../data/' + '120m'+ '/' + data_time, filetype='csv')

    # read data
    data = dict()

    for i in stocks:
        data_df = pd.read_csv('../data/' + '120m' + '/' + data_time + '/' + i + '.csv', index_col=False,
                              encoding='ANSI')

        # convert pandas DataFrame into combination of dictionary and numpy array
        if data_df.empty:
            temp = dict()
            temp['empty'] = True
        else:
            temp = dict()
            temp['empty'] = False
            temp['时间'] = np.array(data_df['时间'].values, dtype=str)
            temp['收盘价'] = np.array(data_df['收盘价'].values, dtype=np.double)
            temp['开盘价'] = np.array(data_df['开盘价'].values, dtype=np.double)
            temp['最高价'] = np.array(data_df['最高价'].values, dtype=np.double)
            temp['最低价'] = np.array(data_df['最低价'].values, dtype=np.double)

        data[i] = temp
    return data
def get_files(path, filetype):
    name = []
    for _, _, files in os.walk(path):
         for i in files:
             if filetype in i:
                  name.append(i.replace('.csv', ''))
    return name

def get_dir(path):
    name = []
    for _, dir, _, in os.walk(path):
        for i in dir:
            name.append(i)
    return name

time_list=get_dir('../data/'+'120m')
for time in time_list[:1]:
    a = data_load(time)
    print(a['SH600000']['收盘价'])


#a = data_load('2019-01-02')
#print(a['SH600000']['时间'])


