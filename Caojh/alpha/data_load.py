import pandas as pd
import numpy as np
import os


def data_load():
    #read stock&datatime
    time_list = get_dir(path='../data/' + '120m')
    stock_list = get_files(path='../data/'+'120m/'+'2019-01-02',filetype='csv')

    # read data
    data = dict()
    for stock in stock_list[:3]:
        #print(stock)
        temp_1 = dict()
        for time in time_list[:3]:
            #print(time)
            data_df = pd.read_csv('../data/' + '120m' + '/' + time + '/' + stock + '.csv', index_col=False,encoding='ANSI')
            if data_df.empty:
                temp = dict()
                temp['empty'] = True
            else:
                temp = dict()
                temp['empty'] = False
                temp['时间'] = np.array(data_df['时间'].values, dtype=str)
                temp['收盘价'] = np.array(data_df['收盘价'].values, dtype=np.double)
                temp['开盘价'] = np.array(data_df['开盘价'].values, dtype=np.double)
                temp['最高价'] = np.array(data_df['最高价'].values, dtype=np.double)
                temp['最低价'] = np.array(data_df['最低价'].values, dtype=np.double)
            temp_1[time] = temp
        data[stock] = temp_1
    return data


def get_dir(path):
    name = []
    for _, dir, _, in os.walk(path):
        for i in dir:
            name.append(i)
    return name


def get_files(path, filetype):
    name = []
    for _, _, files in os.walk(path):
        for i in files:
            if filetype in i:
                name.append(i.replace('.csv', ''))
    return name

a = data_load()
time_list = get_dir(path='../data/' + '120m')
pd.DataFrame.from_dict(a,orient='index')
#print(a['SH600000']['2019-01-02']['收盘价'])


if __name__ == '__main__'
    for time in time_list[:3]:
        b = a['SH600000'][time]
        strategy = Alpha(b)
        print(strategy.alpha101())
