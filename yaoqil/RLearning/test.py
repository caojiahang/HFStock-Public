import numpy as np
import pandas as pd
import time
import random
import os
from pandas import DataFrame

def get_files(path, filetype):
    name = []
    for _, _, files in os.walk(path):
        for i in files:
            if filetype in i:
                name.append(i.replace('.csv', ''))
    return name

data = pd.read_csv('../data/'+'5m/'+'2019-01-02/'+'SH000001'+'.csv',encoding='gbk')
codes_data = get_files(path='../data/'+'120m/'+'2019-01-02',filetype='csv')  #读取所有股票代码

#随机抽取几只股票并提取它们的数据
codes = random.sample(set(codes_data),10)

data2 = data.loc[codes_data.isin(codes)]
for code in codes_data[:3]:
    print(code)
print(data2)
