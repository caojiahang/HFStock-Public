import tensorflow as tf
import numpy as np
import pandas


class PolicyGradient:
    def __init__(self, n_actions, n_features, learning_rate=0.01, reward_decay=0.95, output_graph=True):
        self.n_actions = n_actions
        self.n_features = n_features
        self.lr = learning_rate  # 学习率
        self.gamma = reward_decay  # reward 递减率

        self.ep_observations, self.ep_actions, self.ep_rewards = [], [], []  # 这是我们存储 回合信息的 list

        self._build_net()  # 建立 policy 神经网络

        self.sess = tf.Session()

        self.sess.run(tf.global_variables_initializer())

    def _build_net(self):
        features = len(self.n_features)
        actions = 2
        with tf.name_scope('inputs'):
            self.tf_observations = tf.placeholder(tf.float64, [None, features], name="observations")  # 接收 observation
            self.tf_actions = tf.placeholder(tf.int32, [None, ], name="actions_num")  # 接收我们在这个回合中选过的 actions
            self.tf_values = tf.placeholder(tf.float64, [None, ],
                                            name="actions_value")  # 接收每个 state-action 所对应的 value (通过 reward 计算)

        # fc1
        layer = tf.layers.dense(
            inputs=self.tf_observations,
            units=10,  # 输出个数
            activation=tf.nn.tanh,  # 激励函数
            kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
            bias_initializer=tf.constant_initializer(0.1),
            name='fc1'
        )
        # fc2
        all_act = tf.layers.dense(
            inputs=layer,
            units=actions,  # 输出个数
            activation=None,  # 之后再加 Softmax
            kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
            bias_initializer=tf.constant_initializer(0.1),
            name='fc2'
        )

        self.all_act_prob = tf.nn.softmax(all_act, name='act_prob')  # 激励函数 softmax 出概率
        print(all_act)
        print(self.tf_actions)
        print(self.ep_actions)
        with tf.name_scope('loss'):
            neg_log_prob = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=all_act,
                                                                          labels=self.tf_actions)  # 所选 action 的概率 -log 值
            self.loss = tf.reduce_mean(neg_log_prob * self.tf_values)  # (vt = 本reward + 衰减的未来reward) 引导参数的梯度下降

        with tf.name_scope('train'):
            self.train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss)

    def choose_action(self, observation):
        prob_weights = self.sess.run(self.all_act_prob,
                                     feed_dict={self.tf_observations: observation[np.newaxis, :]})  # 所有 action 的概率
        action = np.random.choice(range(prob_weights.shape[1]), p=prob_weights.ravel())  # 根据概率来选 action
        return action

    def store_transition(self, s, a, r):
        self.ep_observations.append(s)
        self.ep_actions.append(a)
        self.ep_rewards.append(r)

    def learn(self):
        # 衰减, 并标准化这回合的 reward
        discounted_ep_rs_norm = self._discount_and_norm_rewards()  # 功能在下面
        print(discounted_ep_rs_norm)
        #   print(discounted_ep_rs_norm.shape)
        print(np.array(self.ep_observations))
        print(np.array(self.ep_actions))
        # train on episode
        self.sess.run([self.train_op, self.loss], feed_dict={
            self.tf_observations: np.array(self.ep_observations),  # shape=[None, n_obs]
            self.tf_actions: np.array(self.ep_actions),  # shape=[None, ]
            self.tf_values: discounted_ep_rs_norm,  # shape=[None, ]
        })

        ##        self.sess.run(tf.global_variables_initializer())
        self.ep_observations, self.ep_actions, self.ep_rewards = [], [], []  # 清空回合 data
        return discounted_ep_rs_norm  # 返回这一回合的 state-action value

    def _discount_and_norm_rewards(self):
        # discount episode rewards
        discounted_ep_rs = np.zeros_like(self.ep_rewards)
        running_add = 0
        for t in reversed(range(0, len(self.ep_rewards))):
            running_add = running_add * self.gamma + self.ep_rewards[t]
            discounted_ep_rs[t] = running_add
        # normalize episode rewards
        discounted_ep_rs -= np.mean(discounted_ep_rs)
        if np.std(discounted_ep_rs) != 0:
            discounted_ep_rs /= np.std(discounted_ep_rs)
        return discounted_ep_rs


actions = np.array([1., 0., 1.])
features = ['open', 'high', 'low', 'close']
# features=np.array([[3289.75,3295.28,3243.32,3243.76],[3289.75,3295.28,3243.32,3243.76],[3289.75,3295.28,3243.32,3243.76]])
testpg = PolicyGradient(actions, features)
stat = np.array(
    [[3289.75, 3295.28, 3243.32, 3243.76], [3277.52, 3295.87, 3253.04, 3254.22], [3301.61, 3306.75, 3197.33, 3212.75]])
action = testpg.choose_action(stat)
testpg.store_transition(
    [[3289.75, 3295.28, 3243.32, 3243.76], [3277.52, 3295.87, 3253.04, 3254.22], [3301.61, 3306.75, 3197.33, 3212.75]],
    action, 10.)
print(testpg.learn())

