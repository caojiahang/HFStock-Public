import numpy as np
import pandas as pd
import time
import random
from pandas import DataFrame


class Environment:
    def __init__(self):

        print(">> Initialize the Environment.")

    # 获取数据的开始结束时间
    def organ_data(self, start_date, end_date, codes_num):

        # read data
        self.data = pd.read_csv('../data/' + '5m/' + '2019-01-02' + '.csv')
        codes_data_list = get_files(path='../data/' + '5m/' + '2019-01-02', filetype='csv')  # 读取所有股票代码
        codes_data = DataFrame(codes_data_list)
        # 随机抽取几只股票并提取它们的数据
        sample_flag = True
        while sample_flag:
            codes = random.sample(set(codes_data), codes_num)
            data2 = self.data.loc[codes_data.isin(codes)]

            # 生成有效时间
            date_set = set(data2['时间'])
            for code in codes:
                date_set = date_set.intersection((set(data2['时间'])))
            if len(date_set) > 500:
                sample_flag = False

        date_set = date_set.intersection(set(pd.date_range(start_date, end_date)))
        self.date_set = list(date_set)
        self.date_set.sort()

        train_start_time = self.date_set[0]
        train_end_time = self.date_set[int(len(self.date_set) / 6) * 5 - 1]
        test_start_time = self.date_set[int(len(self.date_set) / 6) * 5]
        test_end_time = self.date_set[-1]

        return train_start_time, train_end_time, test_start_time, test_end_time, codes

    # 提取数据并定义初始状态
    def get_data(self, start_time, end_time, window_len, codes):
        self.codes = codes
        for
        self.data = pd.read_csv('../data/' + '5m/' + '2019-01-02' + '.csv')
        codes_data_list = get_files(path='../data/' + '5m/' + '2019-01-02', filetype='csv')  # 读取所有股票代码
        codes_data = DataFrame(codes_data_list)
        # 将指定时间范围内的数据读取
        self.data = self.data[start_time.strftime("%Y-%m-%d"):end_time.strftime("%Y-%m-%d")]
        data = self.data

        # Initialize the state
        self.date_set = pd.date_range(start_time, end_time)
        self.L = int(window_len)

        self.state = []




def get_dir(path):
    name = []
    for _, dir, _, in os.walk(path):
        for i in dir:
            name.append(i)
    return name


def get_files(path, filetype):
    name = []
    for _, _, files in os.walk(path):
        for i in files:
            if filetype in i:
                name.append(i.replace('.csv', ''))
    return name